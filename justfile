_list:
    @just --list

# Format workspace.
fmt:
    cargo +nightly fmt

# Generate readme.
readme:
    cargo rdme --force