# `Rexa`

![Crates.io Version](https://img.shields.io/crates/v/rexa)
[![Documentation](https://docs.rs/rexa/badge.svg)](https://docs.rs/rexa/)
![Version](https://img.shields.io/badge/rustc-1.70+-ab6000.svg)
![License](https://img.shields.io/crates/l/rexa.svg)

<!-- cargo-rdme start -->

Rexa GraphQL and MongoDB code generation library.

<!-- cargo-rdme end -->
